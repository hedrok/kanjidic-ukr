# Licence

See https://www.edrdg.org/edrdg/licence.html for full description.

Mostly it is https://creativecommons.org/licenses/by-sa/4.0/ but with some special additional restrictions.
